var Encore = require('@symfony/webpack-encore');

Encore 
	.setOutputPath('public/build')
	.setPublicPath('/build')

	.addEntry('js/app', './assets/js/app.js')
	.addEntry('js/vuetimer', './assets/js/vue_timer.js')
	.addStyleEntry('css/app', './assets/scss/app.scss')

	.cleanupOutputBeforeBuild()
    .enableBuildNotifications()
	.enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
	.enableSingleRuntimeChunk()

    .enableSassLoader()
	.autoProvidejQuery()
	
	.enableVueLoader()
;

module.exports = Encore.getWebpackConfig();	