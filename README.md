# symfony.timetracker - Планировщик задач

Task timer on Symfiny4 and Vuejs

## 1. Установка
- Сколонировать репозиторий:
`git clone https://bitbucket.org/businesstep/symfony.timetracker.git`

- Перейти в директорию проекта:
`cd symfony.timetracker`

- В файле *.env* указать доступы к БД

- Установить Composer зависимости, выполнив команду:
`composer install`

- Установить yarn зависимости, выполнив команду:
`yarn install`

- Запустить БД миграции, выполнив команду:
`php bin/console docrtine:migrations:diff`
`php bin/console docrtine:migrations:migrate`

- Скомпилировать css и js скрипты, выполнив команду:
`yarn encore dev`

- Запустить локально сервер:
Например, для XAMPP запустить MySQL сервер и запустить PHP командой `php -S localhost:8000 -t public`
