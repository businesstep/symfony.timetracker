import Vue from 'vue';

import Home from './components/Home';

new Vue({
    el: "#app",
    components: {Home},
}).$mount('#app');