<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
	use TimestampableTrait;
	
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

   /**
    * @ORM\Column(type="string", name="title", length=255)
    */
	private $title;
   
   /**
    * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="project")
    * @ORM\JoinColumn(nullable=true)
    */
	private $user;
   
   /**
    * @ORM\OneToMany(targetEntity="App\Entity\Timer", mappedBy="project")
    */
	private $timers;

    public function getId(): ?int
    {
        return $this->id;
    }
	
	public function setId($id)
	{
		$this->id = $id;
	}
   
	public function getTitle()
	{
		return $this->title;
	}
   
	public function setTitle($title)
	{
		$this->title = $title;
	}
   
	public function setUser(User $user)
	{
		$this->user = $user;
	}
   
	public function getTimers()
	{
		return $this->timers;
	}
   
	public function setTimers($timers)
	{
		$this->timers = $timers;
	}
	
	public function __toString()
	{
		return $this->title;
	}
}
