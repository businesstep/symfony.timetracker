<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email уже используется")
 */
class User implements UserInterface, \Serializable
{
	//public const GITHUB_OAUTH = 'Github';
	//public const GOOGLE_OAUTH = 'Google';
	
	public const ROLE_USER = 'ROLE_USER';
	public const ROLE_ADMIN = 'ROLE_ADMIN';

	use TimestampableTrait;
	
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
	
	/**
     * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 * @Assert\Email()
     */
	private $email;
	
	/**
     * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 * @Assert\Length(min=4, max=50)
     */
	private $username;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Length(min=6, max=4096)
     */
	private $plainPassword;
	
	/**
     * @ORM\Column(type="string", length=4096)
     */
	private $password;
	
	/**
     * @ORM\Column(type="boolean", name="is_active")
     */
	private $isActive;

	/**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="user")
     */
	private $project;
	
	/**
	 * @ ORM\Column(type="string", nullable=true)
	 */
	//private $clientId;
	
	/**
	 * @ ORM\Column(type="string")
	 */
	//private $oauthType;
	
	public function __construct()//$clientId, string $email, string $username, string $oauthType, array $roles)
	{
		//$this->clientId = $clientId;
		//$this->email = $email;
		//$this->oauthType = $oauthType;
		//$this->roles = $roles;
		$this->isActive = true;
        $this->project = new ArrayCollection();
	}

	/**
	 * @param int $clientId
	 * @param string $email
	 * @param string $username
	 *
	 * @return User
	 */
/*	public static function fromGithubRequest(int $clientId, string $email, string $username): User
	{
		return new self($clientId, $emai, $username, self::GITHUB_OAUTH, [self::ROLE_USER]);
	}
*/	
	/**
	 * @param string $clientId
	 * @param string $email
	 * @param string $username
	 *
	 * @return User
	 */
/*	public static function fromGoogleRequest(string $clientId, string $email, string $username): User
	{
		return new self($clientId, $email, $username, self::GOOGLE_OAUTH, [self::ROLE_USER]);
	}
*/
    public function getId(): ?int
    {
        return $this->id;
    }
	
	public function setId(int $id): self
	{
		$this->id = $id;
		
		return $this;
	}
	
	public function getUsername(): ?string
	{
		//return $this->username;
		return $this->email;
	}
	
	public function setUsername(string $username): self
	{
		$this->username = $username;
		
		return $this;
	}
	
	public function getEmail(): ?string
	{
		return $this->email;
	}
	
	public function setEmail(string $email): self
	{
		$this->email = $email;
		
		return $this;
	}
	
	public function getIsActive(): ?bool
	{
		return $this->isActive;
	}
	
	public function setIsActive(bool $isActive): self
	{
		$this->isActive = $isActive;
		
		return $this;
	}
	
	public function getProject(): Collection
	{
		return $this->project;
	}

	public function addProject(Project $project): self
    {
        if (!$this->project->contains($project)) {
            $this->project[] = $project;
            $project->setUser($this);
        }

        return $this;
    }
	
	public function removeProject(Project $project): self
    {
        if ($this->project->contains($project)) {
            $this->project->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getUser() === $this) {
                $project->setUser(null);
            }
        }

        return $this;
    }

    public function getSalt()
    {
		return null;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
   
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }
	
	public function getPassword(): ?string
	{
		return $this->password;
	}
	
	public function setPassword(string $password): self
	{
		$this->password = $password;
		
		return $this;
	}
	
	public function getRoles(): array
	{
	    return array('ROLE_USER');
		//return $this->roles;
	}
	
	public function eraseCredentials()
	{}
	
	public function serialize()
	{
		return serialize(array(
		   $this->id,
		   //$this->username,
		   $this->email,
		   $this->password,
	    ));
	}
	
	public function unserialize($serialized)
	{
	    list(
		   $this->id,
		   //$this->username,
		   $this->email,
		   $this->password,
        ) = unserialize($serialized);
	}
	
/*	public function getClientId(): int
	{
		return $this->clientId;
	}
	
	public function getOauthType(): string
	{
		return $this->oauthType;
	}
*/	
}
