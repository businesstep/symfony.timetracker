<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimerRepository")
 */
class Timer
{
	use TimestampableTrait;
	
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

   /**
    * @ORM\Column(type="string", name="title", length=255)
    */
	private $title;
   
   /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="timers")
    * @ORM\JoinColumn(nullable=true)
    */
	private $project;
   
   /**
    * @ORM\Column(type="datetime", name="started_at")
    */
	private $startedAt;
   
   /**
    * @ORM\Column(type="datetime", name="stopped_at", nullable=true)
    */
	private $stoppedAt;
  
    public function getId(): ?int
    {
        return $this->id;
    }
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
   
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getProject()
	{
		return $this->project;
	}
   
	public function setProject($project)
	{
		$this->project = $project;
	}

	public function getStartedAt()
	{
		return $this->startedAt;
	}
   
	public function setStartedAt($startedAt)
	{
		$this->startedAt = $startedAt;
	}

	public function getStoppedAt()
	{
		return $this->stoppedAt;
	}
   
	public function setStoppedAt($stoppedAt)
	{
		$this->stoppedAt = $stoppedAt;
	}

	public function __toString()
	{
		return $this->title;
	}	
}
