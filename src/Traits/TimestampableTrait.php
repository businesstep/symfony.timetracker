<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
   /**
    * @ORM\Column(type="datetime", name="created_at")
    */
	private $createdAt;
   
   /**
    * @ORM\Column(type="datetime", name="updated_at")
    */
	private $updatedAt;	
	
	public function getCreatedAt()
	{
		return $this->createdAt;
	}
   
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
		
		return $this;
	}
   
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}
   
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;
		
		return $this;
	}	
}
