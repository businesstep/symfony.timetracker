<?php

namespace App\Traits;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait SerializeTrait
{
   	protected function serializeObject($object)
	{
		$encoders = new JsonEncoder();
		$normalizers = new ObjectNormalizer();
		
		$normalizers->setCircularReferenceHandler(function ($obj) {
			return $obj->getId();
		});
		
		$serializer = new Serializer(array($normalizers), array($encoders));
		$jsonContent = $serializer->serialize($object, 'json');
		
		return $jsonContent;
	}	
}
