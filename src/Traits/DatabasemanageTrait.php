<?php

namespace App\Traits;

use Doctrine\ORM\EntityManagerInterface;

trait DatabasemanageTrait
{
    protected function updateDatabase(EntityManagerInterface $entityManager, $entity) 
	{
		$entityManager->persist($entity);
		$entityManager->flush();
	}
}
