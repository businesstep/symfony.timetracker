<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Entity\Project;
use App\Traits\DatabasemanageTrait;
use App\Traits\SerializeTrait;

class ProjectController extends AbstractController
{
	use DatabasemanageTrait;
	use SerializeTrait;
	
	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;
	
	/**
	 * @var \Doctrine\Common\Presistence\ObjectRepository 
	 */
	private $projectRepository;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
		$this->projectRepository = $entityManager->getRepository('App:Project');
	}
	
    /**
     * @Route("/project", name="project_index")
     */
    public function index()
    {
		$projects = $this->projectRepository->findByUser($this->getUser()->getId());
		$jsonContent = $this->serializeObject($projects);
		
        return new Response($jsonContent, Response::HTTP_OK);
    }
	
   /**
    * @param Request $request
    * @return Response
    * @Route("/project/create", name="project_create")
    */
	public function create(Request $request)
	{
		$content = json_decode($request->getContent(), true);
				
		if ($content['name']) {
			$project = new Project();
			
			$project->setUser($this->getUser());
			$project->setName($content['name']);
			$project->setTimers([]);
			$project->setCreatedAt(new \DateTime());
			$project->setUpdatedAt(new \DateTime());
			
			$this->updateDatabase($this->entityManager, $project);
			
			// Serialize object into Json format
			$jsonContent = $this->serializeObject($project);
			
			return new Response($jsonContent, Response::HTTP_OK);
		}
		
		return new Response('Error', Response::HTTP_NOT_FOUND);
	}
}
