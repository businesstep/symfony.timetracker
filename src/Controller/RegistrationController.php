<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use App\Traits\DatabasemanageTrait;

class RegistrationController extends AbstractController
{
	use DatabasemanageTrait;
	
	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;
	
	/**
	 * @var \Doctrine\Common\Presistence\ObjectRepository 
	 */
	private $userRepository;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
		$this->userRepository = $entityManager->getRepository('App:User');
	}
	
    /**
     * @Route("/register", name="registration_index")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() /*&& $form->isValid()*/) {
			// encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
			
			$user->setCreatedAt(new \Datetime('now'));
			$user->setUpdatedAt(new \Datetime('now'));

			$this->updateDatabase($this->entityManager, $user);

            return $this->redirectToRoute('security_login');
        }

        return $this->render('registration/index.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }	
}
