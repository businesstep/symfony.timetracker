<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\Timer;
use App\Entity\Project;
use App\Traits\DatabasemanageTrait;
use App\Traits\SerializeTrait;

class TimerController extends AbstractController
{
	use DatabasemanageTrait;
	use SerializeTrait;
	
	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;
	
	/**
	 * @var \Doctrine\Common\Presistence\ObjectRepository 
	 */
	private $projectRepository;


	/**
	 * @var \Doctrine\Common\Presistence\ObjectRepository 
	 */
	private $timerRepository;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
		$this->timerRepository = $entityManager->getRepository('App:Timer');
		$this->projectRepository = $entityManager->getRepository('App:Project');
	}
	
    /**
     * @Route("/project/{id}/timers", name="timer_create")
     */
    public function create(Request $request, Project $project)
    {
        $content = json_decode($request->getContent(), true);

		$timer = new Timer();
		$timer->setName($content['name']);
		//$timer->setUser($this->getUser());
		$timer->setProject($project);
		$timer->setStartedAt(new \DateTime());
		$timer->setCreated(new \DateTime());
		$timer->setUpdated(new \DateTime());
       
		$this->updateDatabase($this->entityManager, $timer);
       
		// Serialize object into Json format
		$jsonContent = $this->serializeObject($timer);
       
		return new Response($jsonContent, Response::HTTP_OK);	   
    }
	
   /**
    * @Route("/project/timers/running", name="timer_running")
    */
	public function running()
	{
		$projects = $this->projectRepository->findByUser($this->getUser()->getId());
		$timer = $this->timerRepository->findRunningTimer(/*$this->getUser()->getId()*/  $projects);
		$jsonContent = $this->serializeObject($timer);
       
		return new Response($jsonContent, Response::HTTP_OK);
	}
	
   /**
    * @Route("/project/{id}/timers/stop", name="timer_stop")
    */
	public function stop()
	{
		$projects = $this->projectRepository->findByUser($this->getUser()->getId());
		$timer = $this->timerRepository->findRunningTimer(/*$this->getUser()->getId()*/ $projects);
		
		if ($timer) {
			$timer->setStoppedAt(new \DateTime());
			$this->updateDatabase($timer);
		}
		
		// Serialize object into Json format
		$jsonContent = $this->serializeObject($timer);
		
		return new Response($jsonContent, Response::HTTP_OK);
	}
}
